#include <iostream>
#include <cstring>
#include <iomanip>
#include <stdexcept>
#include <zlib.h>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>
#include "base64.h"
#include <boost/asio.hpp>
#include <csignal>
#include <unistd.h>
#include <sys/types.h>
using namespace std;
using namespace boost;
using namespace boost::asio::ip;
asio::io_service ioserv;
namespace fs = boost::filesystem;
unsigned short port = 15945;

void sigint_handler(int x)
{
	cerr << endl << "Caught SIGINT. Exiting." << x << endl;
	throw std::runtime_error("Received SIGINT, exiting with exit code 0.");
}

/** Compress a STL string using zlib with given compression level and return
  * the binary data. */
std::string zcompress(const std::string& str,
							int compressionlevel = Z_BEST_COMPRESSION)
{
	z_stream zs;                        // z_stream is zlib's control structure
	memset(&zs, 0, sizeof(zs));

	if (deflateInit(&zs, compressionlevel) != Z_OK)
		throw(std::runtime_error("deflateInit failed while compressing."));

	zs.next_in = (Bytef*)str.data();
	zs.avail_in = str.size();           // set the z_stream's input

	int ret;
	char outbuffer[32768];
	std::string outstring;

	// retrieve the compressed bytes blockwise
	do {
		zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
		zs.avail_out = sizeof(outbuffer);

		ret = deflate(&zs, Z_FINISH);

		if (outstring.size() < zs.total_out) {
			// append the block to the output string
			outstring.append(outbuffer,
							 zs.total_out - outstring.size());
		}
	} while (ret == Z_OK);

	deflateEnd(&zs);

	if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
		std::ostringstream oss;
		oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
		throw(std::runtime_error(oss.str()));
	}
	return base64_encode(reinterpret_cast<const unsigned char*>(outstring.c_str()), outstring.length());
	//return outstring;
}

/** Decompress an STL string using zlib and return the original data. */
std::string zdecompress(const std::string& base64_str)
{
	const string str = base64_decode(base64_str);
	z_stream zs;                        // z_stream is zlib's control structure
	memset(&zs, 0, sizeof(zs));

	if (inflateInit(&zs) != Z_OK)
		throw(std::runtime_error("inflateInit failed while decompressing."));

	zs.next_in = (Bytef*)str.data();
	zs.avail_in = str.size();

	int ret;
	char outbuffer[32768];
	std::string outstring;

	// get the decompressed bytes blockwise using repeated calls to inflate
	do {
		zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
		zs.avail_out = sizeof(outbuffer);

		ret = inflate(&zs, 0);

		if (outstring.size() < zs.total_out) {
			outstring.append(outbuffer,
							 zs.total_out - outstring.size());
		}

	} while (ret == Z_OK);

	inflateEnd(&zs);

	if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
		std::ostringstream oss;
		oss << "Exception during zlib decompression: (" << ret << ") "
			<< zs.msg;
		cerr << oss.str() << endl;
		//throw(std::runtime_error(oss.str()));
	}

	return outstring;
}

void ctplisten()
{
	try
	{
		tcp::acceptor a(ioserv, tcp::endpoint(tcp::v4(), port));
		tcp::socket sock(ioserv);
		cout << "File to read message from: ";
		string filename;
		cin >> filename;
		size_t fsize = fs::file_size(filename);
		cout << "File size is " << fsize << " bytes." << endl;
		char* msgc = (char*) malloc(fsize + 50);
		fstream file;
		cout << "Reading file into memory." << endl;
		file.open(filename, ios_base::in);
		file.read(msgc, fsize);
		file.close();
		string msg = msgc;
		delete[] msgc;
		cout << "Finished reading file. Compressing data." << endl;
		string cmp_msg = zcompress(msg);
		msg = "";
		cout << "Finished compressing data. Compressed data size is " << cmp_msg.length() << " bytes." << endl;
		cout << "Awaiting connection." << endl;
		a.accept(sock);
		boost::system::error_code ie;
		cout << "Client connected to server and is awaiting message. Client IP: " << sock.remote_endpoint().address().to_string() << endl;
		cout << "[CTP/SOCKET] Sending compressed message." << endl;
		asio::write(sock, asio::buffer(cmp_msg), ie);
		cout << "[CTP/SOCKET] Done." << endl;
		cout << "Message sent to client at " << sock.remote_endpoint().address().to_string() << endl;
		sock.close();
	}
	catch (std::exception& e)
	{
		cerr << e.what() << endl;
		throw std::runtime_error("Server closed.");
	}
}

int main()
{
	cout << "CTP server starting on port " << port << endl;

	signal(SIGINT, sigint_handler);
	cout << "Ctrl+C will terminate the server." << endl;
	cout << "This program's PID is " << getpid() << endl;
	while (true)
	{
		try
		{
			ctplisten();
		}
		catch (std::exception& e)
		{
			cerr << e.what() << endl;
			return 0;
		}
	}
	return 0;
}

